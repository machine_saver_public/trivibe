# Trivibe

## Writing Alarms Modbus Registers
Step 1 Write to Unlock Control Register 

| Value | Offset/Start Address | Number of Registers | Data Type | Measureand | Adjustment to Scale | Options |  
| ----- | -------------------- | ------------------- | --------- | ---------- | ------------------- | ------- |  
| Control Register | 1 | 1 | Unsigned Integer | n/a | None | 42074 - Unlock Alarm Configuration <br> 42075 - Lock Alarm Configuration |  

Alarm Configuration  

| Value | Offset/Start Address | Number of Registers | Data Type | Measureand | Adjustment to Scale | Options |  
| ----- | -------------------- | ------------------- | --------- | ---------- | ------------------- | ------- |  
| Alarm 1 Axis | 312 | 1 | Unsigned Integer | n/a | None | 1 - Axis 1 <br> 2 - Axis 2 <br> 3 - Axis 3 |  
| Alarm 1 Type | 313 | 1 | Unsigned Integer | n/a | None | 1 - Acceleration <br>  2 - Velocity | 
| Alarm 1 High Limit | 316 | 2 | Floating Point | n/a | None | n/a |  
| Alarm 2 Axis | 322 | 1 | Unsigned Integer | n/a | None | 1 - Axis 1 <br> 2 - Axis 2 <br> 3 - Axis 3 |  
| Alarm 2 Type | 323 | 1 | Unsigned Integer | n/a | None | 1 - Acceleration <br>  2 - Velocity | 
| Alarm 2 High Limit | 326 | 2 | Floating Point | n/a | None | n/a |  
| Alarm 3 Axis | 332 | 1 | Unsigned Integer | n/a | None | 1 - Axis 1 <br> 2 - Axis 2 <br> 3 - Axis 3 |  
| Alarm 3 Type | 333 | 1 | Unsigned Integer | n/a | None | 1 - Acceleration <br>  2 - Velocity | 
| Alarm 3 High Limit | 336 | 2 | Floating Point | n/a | None | n/a |  
| Alarm 4 Axis | 342 | 1 | Unsigned Integer | n/a | None | 4 - Temperature |  
| Alarm 4 Type | 343 | 1 | Unsigned Integer | n/a | None | 4 - Temperature | 
| Alarm 4 High Limit | 346 | 2 | Floating Point | n/a | None | n/a |  

Lock Control Register  

| Value | Offset/Start Address | Number of Registers | Data Type | Measureand | Adjustment to Scale | Options |  
| ----- | -------------------- | ------------------- | --------- | ---------- | ------------------- | ------- |  
| Control Register | 1 | 1 | Unsigned Integer | n/a | None | 42074 - Unlock Alarm Configuration <br> 42075 - Lock Alarm Configuration |  

| Value | Offset/Start Address | Number of Registers | Data Type | Measureand | Adjustment to Scale |
| ----- | ------ | ------------------- | --------- | ---------- | ------------------- |
| Serial Number | 26 | 2 | Unsigned Integer | n/a | None |
| Uptime - Minutes | 5 | 1 | Unsigned Integer | n/a | None |
| Uptime - Hours | 6 | 1 | Unsigned Integer | n/a | None |
| Uptime - Days | 7 | 1 | Unsigned Integer | n/a | None |
| Temperature | 31 | 1 | Signed Integer | Celsius | Return Value / 10 |
| Axis 1 - Acceleration | 172 | 2 | Floating Point | g (RMS) | None |
| Axis 2 - Acceleration | 174 | 2 | Floating Point | g (RMS) | None |
| Axis 3 - Acceleration | 176 | 2 | Floating Point | g (RMS) | None |
| Axis 1 - Velocity | 178 | 2 | Floating Point | ips (RMS) | None |
| Axis 2 - Velocity | 180 | 2 | Floating Point | ips (RMS) | None |
| Axis 3 - Velocity | 182 | 2 | Floating Point | ips (RMS) | None |